import * as firebase from 'firebase/app'

const config = {"apiKey":"AIzaSyAKFNYIZw7oJsnXtx86dujSQY5-tzoNAAA","authDomain":"desikit.firebaseapp.com","databaseURL":"https:\u002F\u002Fdesikit.firebaseio.com","projectId":"desikit","storageBucket":"desikit.appspot.com","messagingSenderId":"475295116555","appId":"1:475295116555:web:b8c64a4d1b94b8af3b860e","measurementId":"G-9S5H9GNHNE"}

export default async ({ res }, inject) => {
  if (!firebase.apps.length) {
    firebase.initializeApp(config)
  }
  const session = firebase.apps[0]

  /** --------------------------------------------------------------------------------------------- **/
  /** ----------------------------------- FIREBASE PERFORMANCE ------------------------------------ **/
  /** --------------------------------------------------------------------------------------------- **/

  // Firebase Performance can only be initiated on client side
  if(process.client) {
    await import(/* webpackChunkName: 'firebase-performance' */'firebase/performance')

    const firePerf = session.performance()
    const firePerfObj = firebase.performance
    inject('firePerf', firePerf)
    inject('firePerfObj', firePerfObj)
  }

  /** --------------------------------------------------------------------------------------------- **/
  /** ----------------------------------- FIREBASE ANALYTICS -------------------------------------- **/
  /** --------------------------------------------------------------------------------------------- **/

  // Firebase Analytics can only be initiated on the client side
  if (process.client) {
    await import(/* webpackChunkName: 'firebase-analytics' */'firebase/analytics')

    const fireAnalytics = session.analytics()
    const fireAnalyticsObj = firebase.analytics
    inject('fireAnalytics', fireAnalytics)
    inject('fireAnalyticsObj', fireAnalyticsObj)
  }
}
