import 'dotenv/config'

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  /*
   ** Global CSS
   */
  css: [ 
    '@/assets/scss/custom.scss'
   ],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt/content
    '@nuxt/content',
    '@nuxtjs/auth',
    [
      '@nuxtjs/firebase',
      {
        config: {
          production: {
            apiKey: "AIzaSyAKFNYIZw7oJsnXtx86dujSQY5-tzoNAAA",
            authDomain: "desikit.firebaseapp.com",
            databaseURL: "https://desikit.firebaseio.com",
            projectId: "desikit",
            storageBucket: "desikit.appspot.com",
            messagingSenderId: "475295116555",
            appId: "1:475295116555:web:b8c64a4d1b94b8af3b860e",
            measurementId: "G-9S5H9GNHNE"
          },
          development: {
            apiKey: "AIzaSyAKFNYIZw7oJsnXtx86dujSQY5-tzoNAAA",
            authDomain: "desikit.firebaseapp.com",
            databaseURL: "https://desikit.firebaseio.com",
            projectId: "desikit",
            storageBucket: "desikit.appspot.com",
            messagingSenderId: "475295116555",
            appId: "1:475295116555:web:b8c64a4d1b94b8af3b860e",
            measurementId: "G-9S5H9GNHNE"
          }
        },
        customEnv: false,
        onFirebaseHosting: false,
        services: {
          performance: true,
          analytics: true
        }
      }
    ]
  ],
  auth: {
    redirect: {
      login: '/', // redirect user when not connected
      callback: '/auth/signed-in'
    },
    strategies: {
      local: false,
      auth0: {
        domain: 'desica.eu.auth0.com',
        client_id: process.env.AUTH0_CLIENT_ID
      }
    }
  },
  bootstrapVue: {
    bootstrapCSS: false, 
    bootstrapVueCSS: false
  },
  module: {
    rules: [
        {
           test: /\.s[ac]ss$/i,
           use: ['style-loader','css-loader','sass-loader',],
         },  
    ],
},
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {},
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {},
}
