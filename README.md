# Starter template

## Project setup

Fork the repo, then make sure that you have updated package.json with the app name, and added .env with auth0 info.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```

## Generate files, ready for production

```bash
# Generate, ready for pruduction
$ npm run build
$ npm run export

# Serve the build project with hot reload at localhost:3000
$ npm run start
```

## Deploy to Vercel
Make sure to commit your changes first.

```bash
# Deploy
$ now

# Deploy to production
$ now --prod
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org), [Vercel docs](https://vercel.com/docs) and [Auth0 docs](https://auth0.com/docs).
